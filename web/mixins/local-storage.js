import store from 'store2'

export default {
  methods: {
    localStorage(key, ...params) {
      return store([this.namespace, key].join('-'), ...params)
    },
  },
}
