/**
 * | => array separators
 * `` => inline code
 *
 * @param {value} - Raw string content to be interpreted
 * @returns
 */
export default function interpretVariables(value) {
  let content = value

  if (!value) {
    return ''
  }

  content = content.replace(/\s\|/g, ',')
  content = content.replace(/^`/g, '<code>')
  content = content.replace(/`$/g, '</code>')

  return content
}
