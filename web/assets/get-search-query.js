export function getSearchQuery() {
  const params = new URLSearchParams(window.location.search)
  return params.get('q') ?? ''
}

export function setSearchQuery(value) {
  const params = new URLSearchParams(window.location.search)

  if (value) {
    params.set('q', value)
  } else {
    params.delete('q')
  }

  const queryString = params.toString() ? `?${params.toString()}` : ''

  window.history.replaceState(
    {},
    document.title,
    `${window.location.pathname}${queryString}`
  )
}
