# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/loader"

RSpec.describe Loader do
  let(:instance) { described_class.new }
  let(:tree_api_response) do
    [
      {
        "flatPath" => "file_path_1.yml"
      }, {
        "flatPath" => "file_path_2.yml"
      }
    ]
  end
  let(:blob_api_response) do
    [
      {
        "rawBlob" => "raw_blob_1",
        "name" => "file_path_1.yml"
      }, {
        "rawBlob" => "raw_blob_2",
        "name" => "file_path_2.yml"
      }, {
        "rawBlob" => "---\nintroduced_by_url: www.gitlab.com",
        "name" => "file_path_3.yml"
      }
    ]
  end
  let(:file_commits_api1_response) do
    [
      {
        "id" => "sha_1"
      },
      {
        "id" => "sha_2"
      }
    ]
  end
  let(:file_commits_api2_response) do
    [
      {
        "id" => "sha_3"
      },
      {
        "id" => "sha_4"
      }
    ]
  end
  let(:commit_mrs_api1_response) do
    [
      {
        "id" => "mr_id_1",
        "web_url" => "mr_web_url"
      }
    ]
  end
  let(:commit_mrs_api2_response) { [] }

  before do
    stub_const("#{described_class}::SERVICE_PING_METRIC_FOLDERS", ["metrics"])

    described_class::CE_EE_FOLDERS.each do |folder_name|
      tree_api = double(TreeApi)
      allow(TreeApi).to receive(:new).and_return(tree_api)
      allow(tree_api).to receive(:fetch).and_return(tree_api_response)

      blob_api = double(BlobApi)
      allow(BlobApi).to receive(:new).and_return(blob_api)
      allow(blob_api).to receive(:fetch) { blob_api_response.map &:dup }

      file_commits_api1 = double(FileCommitsApi)
      file_commits_api2 = double(FileCommitsApi)

      allow(FileCommitsApi).to receive(:new) do |file_path|
        next file_commits_api1 if file_path.end_with? "file_path_1.yml"

        file_commits_api2 if file_path.end_with? "file_path_2.yml"
      end

      allow(file_commits_api1).to receive(:get).and_return(file_commits_api1_response)
      allow(file_commits_api2).to receive(:get).and_return(file_commits_api2_response)

      commit_mrs_api1 = double(CommitMergeRequestsApi)
      commit_mrs_api2 = double(CommitMergeRequestsApi)

      allow(CommitMergeRequestsApi).to receive(:new) do |sha|
        next commit_mrs_api1 if sha == "sha_2"

        commit_mrs_api2 if sha == "sha_4"
      end

      allow(commit_mrs_api1).to receive(:get).and_return(commit_mrs_api1_response)
      allow(commit_mrs_api2).to receive(:get).and_return(commit_mrs_api2_response)
    end
  end

  after do
    FileUtils.rm_f('service_ping_data.json')
    FileUtils.rm_f('event_data.json')
  end

  describe "#fetch" do
    it "uses TreeApi with proper arguments" do
      instance.fetch

      described_class::CE_EE_FOLDERS.each do |folder_name|
        expect(TreeApi).to have_received(:new).with(File.join(folder_name, "metrics"))
      end
    end

    it "uses BlobApi with proper arguments" do
      instance.fetch

      expect(BlobApi).to have_received(:new).with(%w(file_path_1.yml file_path_2.yml)).twice
    end

    it "uses FileCommitsApi with proper arguments" do
      instance.fetch

      described_class::CE_EE_FOLDERS.each do |folder_name|
        expect(FileCommitsApi).to have_received(:new).with(File.join(folder_name, "metrics/file_path_1.yml"))
        expect(FileCommitsApi).to have_received(:new).with(File.join(folder_name, "metrics/file_path_2.yml"))
      end
    end

    it "uses CommitMergeRequestsApi with proper arguments" do
      instance.fetch

      count = described_class::CE_EE_FOLDERS.count

      expect(CommitMergeRequestsApi).to have_received(:new).with("sha_2").exactly(count).times
      expect(CommitMergeRequestsApi).to have_received(:new).with("sha_4").exactly(count).times
    end

    it "loads proper data" do
      instance.fetch
      instance.save

      expect(instance.load).to match_array(
        [
          { "name" => "file_path_1.yml", "rawBlob" => "raw_blob_1", "autoloaded_introduced_by_url" => "mr_web_url" },
          { "name" => "file_path_2.yml", "rawBlob" => "raw_blob_2" },
          { "name" => "file_path_3.yml", "rawBlob" => "---\nintroduced_by_url: www.gitlab.com" }
        ] * 2
      )
    end

    context "events type" do
      let(:instance) { described_class.new(type: :events) }

      it "uses TreeApi with proper arguments" do
        instance.fetch

        described_class::CE_EE_FOLDERS.each do |folder_name|
          expect(TreeApi).to have_received(:new).with(File.join(folder_name, "events"))
        end
      end
    end

    context "files need to be split into separate batches" do
      let(:tree_api_response) do
        (Loader:: FILE_BATCH_SIZE + 1).times.map do |index|
          {
            "flatPath" => "file_path_#{index}.yml"
          }
        end
      end

      before do
        stub_const("#{described_class}::FILE_BATCH_SIZE", 15)
      end

      it "loads the files in batches" do
        instance.fetch

        file_paths = tree_api_response.map { |file| file["flatPath"] }
        expect(BlobApi).to have_received(:new).with(file_paths[0..(Loader:: FILE_BATCH_SIZE - 1)]).exactly(2).times
        expect(BlobApi).to have_received(:new).with(file_paths[Loader:: FILE_BATCH_SIZE..-1]).exactly(2).times
      end
    end
  end
end
