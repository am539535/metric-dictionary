# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/api/rest_api"

RSpec.describe RestApi do
  context "subclass doesn't have #path implemented" do
    let(:child_class) do
      Class.new(described_class) do
        def params
          {}
        end
      end
    end

    it "raises an error" do
      expect { child_class.new.get }.to raise_error(NotImplementedError)
    end
  end

  context "subclass doesn't have #params implemented" do
    let(:child_class) do
      Class.new(described_class) do
        def path
          '/repository/commits'
        end
      end
    end

    it "raises an error" do
      expect { child_class.new.get }.to raise_error(NotImplementedError)
    end
  end

  context "subclass has all required methods implemented" do
    let(:child_class) do
      Class.new(described_class) do
        def path
          '/repository/commits'
        end

        def params
          { param1: :value1 }
        end
      end
    end
    let(:expected_url) { rest_api_url(page: 1) }
    let(:response) { [{ 'id' => 1 }] }
    let(:instance) { child_class.new }

    before do
      stub_request(:get, expected_url).to_return(status: 200, body: response.to_json, headers: {})
    end

    it "hits the proper endpoint" do
      instance.get

      expect(WebMock).to have_requested(:get, expected_url).once
    end

    it "returns the proper response" do
      expect(instance.get).to eq(response)
    end

    context "when pagination is needed" do
      let(:response) { [{ 'id' => 1 }] * 50 }
      let(:response2) { [{ 'id' => 2 }] * 50 }
      let(:response3) { [{ 'id' => 2 }] * 49 }

      before do
        stub_request(:get, rest_api_url(page: 2)).to_return(status: 200, body: response2.to_json, headers: {})
        stub_request(:get, rest_api_url(page: 3)).to_return(status: 200, body: response3.to_json, headers: {})
      end

      it "performs all needed requests" do
        instance.get

        expect(WebMock).to have_requested(:get, rest_api_url(page: 1)).once
        expect(WebMock).to have_requested(:get, rest_api_url(page: 2)).once
        expect(WebMock).to have_requested(:get, rest_api_url(page: 3)).once
      end
    end
  end

  def rest_api_url(page:)
    "#{described_class::API_PATH}/repository/commits?page=#{page}&param1=value1&per_page=50"
  end
end
