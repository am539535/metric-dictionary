# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/api/commit_merge_requests_api"

RSpec.describe CommitMergeRequestsApi do
  let(:instance) { described_class.new(commit_sha) }
  let(:commit_sha) { "sha123" }
  let(:expected_url) { %r{/projects/gitlab-org%2Fgitlab/repository/commits/#{commit_sha}/merge_requests} }
  let(:response) { [merge_request1, merge_request2] }
  let(:merge_request1) { { 'id' => 'mr1' } }
  let(:merge_request2) { { 'id' => 'mr2' } }

  before do
    stub_request(:get, expected_url).to_return(status: 200, body: response.to_json, headers: {})
  end

  it "hits the proper endpoint" do
    instance.get

    expect(WebMock).to have_requested(:get, expected_url).once
  end

  it "returns the proper response" do
    expect(instance.get).to eq(response)
  end
end
