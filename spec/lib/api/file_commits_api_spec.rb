# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/api/file_commits_api"

RSpec.describe FileCommitsApi do
  let(:instance) { described_class.new(file_path) }
  let(:file_path) { "dir/file.yml" }
  let(:expected_url) { %r{/projects/gitlab-org%2Fgitlab/repository/commits.*path=dir/file.yml} }
  let(:response) { [commit1, commit2] }
  let(:commit1) { { 'id' => 'sha1' } }
  let(:commit2) { { 'id' => 'sha2' } }

  before do
    stub_request(:get, expected_url).to_return(status: 200, body: response.to_json, headers: {})
  end

  it "hits the proper endpoint" do
    instance.get

    expect(WebMock).to have_requested(:get, expected_url).once
  end

  it "returns the proper response" do
    expect(instance.get).to eq(response)
  end
end
