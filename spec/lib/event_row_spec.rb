# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/event_row"
require "json"

RSpec.describe EventRow do
  subject(:row) { described_class.new(data) }
  let(:tiers) { ["free", "premium", "ultimate"] }
  let(:distributions) { ["ee", "ce"] }

  let(:data) do
    {
      "id"=> "gid://gitlab/Blob/834d13968d0fd15da47e675937083218294d5b9f",
      "name"=> "1643968255_projectsnew_select_deployment_target.yml",
      "webPath"=> "/gitlab-org/gitlab/-/blob/master/config/events/1643968255_projectsnew_select_deployment_target.yml",
      "rawBlob"=> {
        "description"=> "Deployment target option selected from new project creation form",
        "category"=> "projects:new",
        "action"=> "select_deployment_target",
        "label_description"=> "new_project_deployment_target",
        "property_description"=> "selected option (string)",
        "product_section"=> "ops",
        "product_stage"=> "configure",
        "product_group"=> "group::configure",
        "product_category" => nil,
        "milestone"=> "14.8",
        "introduced_by_url"=> "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79873",
        "distributions"=> distributions,
        "tiers"=> tiers
      }
    }
  end

  it "returns definition basename as key" do
    expect(row.key).to eq("1643968255_projectsnew_select_deployment_target")
  end

  it "returns distribution" do
    expect(row.distribution).to eq("ee, ce")
  end

  describe "#tier" do
    subject { row.tier }

    context "with all tiers" do
      let(:tiers) { ["free", "premium", "ultimate"] }

      it "returns free" do
        is_expected.to eq("free")
      end
    end

    context "with premium and ultimate" do
      let(:tiers) { ["premium", "ultimate"] }

      it "returns premium" do
        is_expected.to eq("premium")
      end
    end

    context "with ultimate" do
      let(:tiers) { ["ultimate"] }

      it { is_expected.to eq("ultimate") }
    end
  end

  describe "#web_path" do
    subject { row.web_path }

    it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/events/1643968255_projectsnew_select_deployment_target.yml") }
  end
end
