# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/metric_row"
require "json"

RSpec.describe MetricRow do
  subject(:row) { described_class.new(data) }
  let(:tier) { ["free", "premium", "ultimate"] }
  let(:distribution) { ["ee", "ce"] }
  let(:instrumentation_class) { "CountUsersCreatingCiBuildsMetric" }

  let(:data) do
     {
       "id"=>"gid://gitlab/Blob/edf11470cb301a7e614b8bd8605fd289b8049233",
        "name"=>"20210201124934_deployments.yml",
        "webPath"=>"/gitlab-org/gitlab/-/blob/master/config/metrics/counts_all/20210201124934_deployments.yml",
        "rawBlob"=> {
          "data_category"=>"optional",
          "key_path"=>"counts.deployments",
          "description"=>"Total deployments count",
          "product_section"=>"ops",
          "product_stage"=>"release",
          "product_group"=>"group::release",
          "value_type"=>"number",
          "status"=>"active",
          "milestone"=>"8.12",
          "introduced_by_url"=>"https://gitlab.com/gitlab-org/gitlab/-/merge_requests/735",
          "time_frame"=>"all",
          "data_source"=>"database",
          "instrumentation_class" => instrumentation_class,
          "distribution"=> distribution,
          "tier"=> tier,
          "performance_indicator_type"=>["smau", "gmau", "paid_gmau"],
          "options"=>["events" => ["event_a", "event_b"]]
        }
      }
  end

  it "formats the creation date" do
    expect(row.date_created).to eq("2021-02-01")
  end

  it "returns performance_indicator_type" do
    expect(row.performance_indicator_type).to eq("smau, gmau, paid_gmau")
  end

  it "returns distribution" do
    expect(row.distribution).to eq("ee, ce")
  end

  it "returns a valid sisense_query" do
    expected_query = <<-SQL
      SELECT ping_created_at,
             raw_usage_data_payload:counts:deployments AS deployments
      FROM common_prep.prep_usage_ping
      WHERE dim_instance_id IN ('ea8bf810-1d6f-4a6a-b4fd-93e8cbd8b57f')
      ORDER BY ping_created_at DESC
      LIMIT 5
    SQL

    expect(row.sisense_query).to eq(expected_query)
  end

  context "returns the lowest tier" do
    subject { row.tier }

    context "with all tiers" do
      let(:tier) { ["free", "premium", "ultimate"] }

      it { is_expected.to eq("free") }
    end

    context "with premium and ultimate" do
      let(:tier) { ["premium", "ultimate"] }

      it { is_expected.to eq("premium") }
    end

    context "with ultimate" do
      let(:tier) { ["ultimate"] }

      it { is_expected.to eq("ultimate") }
    end
  end

  context "instrumentation class" do
    subject { row.instrumentation_class_link }

    context "in CE and EE" do
      let(:distribution) { ["ce", "ee"] }

      it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/usage/metrics/instrumentations/count_users_creating_ci_builds_metric.rb") }
    end

    context "only in EE" do
      let(:distribution) { ["ee"] }

      it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/gitlab/usage/metrics/instrumentations/count_users_creating_ci_builds_metric.rb") }
    end

    context "instrumentation class within a module" do
      let(:instrumentation_class) { "ModuleName::CountUsersCreatingCiBuildsMetric" }

      it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/usage/metrics/instrumentations/module_name/count_users_creating_ci_builds_metric.rb") }
    end
  end

  describe "#autoloaded_introduced_by_url" do
    subject { row.autoloaded_introduced_by_url }

    it { is_expected.to be_nil }

    context "with autoloaded url" do
      let(:row) { described_class.new(data.merge("autoloaded_introduced_by_url" => "autoloaded.url")) }

      it { is_expected.to eq("autoloaded.url") }
    end
  end
end
