# frozen_string_literal: true

require "pry"
require 'webmock/rspec'

RSpec.configure do |config|
  config.order = "random"
end
