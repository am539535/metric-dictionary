require 'json'
require_relative 'api/blob_api'
require_relative 'api/tree_api'
require_relative 'api/file_commits_api'
require_relative 'api/commit_merge_requests_api'
require_relative 'parser'
require_relative 'metric_row'
require_relative 'event_row'

class Loader
  SERVICE_PING_METRIC_FOLDERS = [
    'metrics/counts_all',
    'metrics/counts_7d',
    'metrics/counts_28d',
    'metrics/settings',
    'metrics/license',
  ]

  EVENT_FOLDERS = [
    'events'
  ]

  CE_EE_FOLDERS = [
    "config/",
    "ee/config/"
  ]
  FILE_BATCH_SIZE = 20

  def initialize(type: :service_ping)
    if type != :service_ping && type != :events
      raise ArgumentError.new("Unsupported type")
    end

    @files = []
    @type = type
  end

  def fetch
    folders_to_fetch.each do |type|
      CE_EE_FOLDERS.each do |folder|
        load_files([folder, type].join)
      end
    end

    @files = @files.flatten
  end

  def save
    File.write(filename, JSON.generate(@files))
  end

  def fetched?
    File.exist?(filename)
  end

  def refetch
    fetch
    save
  end

  def load
    JSON.parse(File.read(filename))
  end

  private

  def load_files(directory)
    # Slicing to prevent graphql-query complexity failures
    TreeApi.new(directory).fetch.map { |f| f["flatPath"] }.each_slice(FILE_BATCH_SIZE) do |file_paths|
      pp "Loading next batch #{file_paths}"
      raw_files = BlobApi.new(file_paths).fetch
      raw_files.each do |raw_file|
        handle_raw_file(directory, raw_file)
      end

      @files.push(raw_files)
    end
  end

  def handle_raw_file(directory, raw_file)
    parsed_attributes = Parser.new([raw_file.dup]).parse.first

    return if parsed_attributes.fetch("rawBlob")[MetricRow::INTRODUCED_BY_URL_KEY]

    file_path = File.join(directory, raw_file.fetch("name"))
    merge_requests = load_merge_requests(file_path)

    return pp "More than one associated MRs for file #{file_path}" if merge_requests.count > 1
    return pp "No associated MRs for file #{file_path}" if merge_requests.count < 1

    raw_file[MetricRow::AUTOLOADED_INTRODUCED_BY_URL_KEY] = merge_requests.first.fetch("web_url")
  end

  def load_merge_requests(file_path)
    creation_commit = FileCommitsApi.new(file_path).get.last
    CommitMergeRequestsApi.new(creation_commit.fetch("id")).get
  end

  def filename
    @type == :service_ping ? 'service_ping_data.json' : 'event_data.json'
  end

  def folders_to_fetch
    @type == :service_ping ? SERVICE_PING_METRIC_FOLDERS : EVENT_FOLDERS
  end
end
