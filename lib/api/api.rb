require 'json'
require 'net/http'

class Api
  URL = URI('https://gitlab.com/api/graphql')
  HEADER = {'Content-Type' => 'application/json'}
  MAX_PAGE = 50

  def node_path
    raise NotImplementedError
  end

  def params
    raise NotImplementedError
  end

  def query
    raise NotImplementedError
  end

  def fetch
    cursor = ''

    MAX_PAGE.times.each_with_object([]) do |_, result|
      template = query % { after: cursor, params: params }
      request_data = { query: template }.to_json
      response = Net::HTTP.post(URL, request_data, HEADER)
      json = JSON.parse(response.body)

      result.concat(parse(json))

      page_info = json.dig('data', *page_info_path, 'pageInfo')

      break result unless page_info['hasNextPage']

      cursor = page_info['endCursor']
    end

  end
end
