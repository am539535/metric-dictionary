require_relative 'rest_api'
require 'cgi'

class CommitMergeRequestsApi < RestApi
  PROJECT_PATH = "gitlab-org/gitlab"

  def initialize(commit_sha)
    @commit_sha = commit_sha
  end

  private

  attr_reader :commit_sha

  def path
    "/projects/#{encoded_project_path}/repository/commits/#{commit_sha}/merge_requests"
  end

  def encoded_project_path
    CGI.escape(PROJECT_PATH)
  end

  def params
    {}
  end
end
