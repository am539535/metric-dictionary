require 'json'
require 'net/http'

class RestApi
  API_PATH = 'https://gitlab.com/api/v4'
  HEADER = {'Content-Type' => 'application/json'}
  MAX_PAGE = 50
  PER_PAGE = 50

  def get
    page = 1

    MAX_PAGE.times.each_with_object([]) do |_, result|
      response = Net::HTTP.get(uri(page), HEADER)
      current_result = JSON.parse(response)

      result.concat(current_result)

      break result unless current_result.count == PER_PAGE

      page += 1
    end
  end

  private

  def uri(page)
    uri = URI(API_PATH)
    uri.path += path

    request_data = params.merge(per_page: PER_PAGE, page: page)
    uri.query = URI.encode_www_form(request_data)

    uri
  end

  def path
    raise NotImplementedError
  end

  def params
    raise NotImplementedError
  end
end
