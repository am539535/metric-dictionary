require 'json'
require 'yaml'

class Parser
  def initialize(data)
    @data = data
  end

  def parse
    @data.map do |d|
      d["rawBlob"] = YAML.load(d["rawBlob"])
      d
    end
  end
end
