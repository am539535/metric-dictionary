# frozen_string_literal: true

require 'uri'

class EventRow
  SERIALIZED_ATTRIBUTES = %i[description action category label_description property_description
                             value_description extra_properties identifiers introduced_by_url product_stage
                             product_section product_group milestone].freeze
  SERIALIZED_METHODS = %i[key distribution web_path tier].freeze

  GITLAB_BASE_URL = 'https://gitlab.com/gitlab-org/gitlab/-/blob/master/'

  attr_reader(*SERIALIZED_ATTRIBUTES)

  def initialize(data)
    @data = data

    @description = data['rawBlob']['description']
    @action = data['rawBlob']['action']
    @category = data['rawBlob']['category']
    @label_description = data['rawBlob']['label_description']
    @property_description = data['rawBlob']['property_description']
    @value_description = data['rawBlob']['value_description']
    @extra_properties = data['rawBlob']['extra_properties']
    @identifiers = data['rawBlob']['identifiers']
    @introduced_by_url = data['rawBlob']['introduced_by_url']
    @product_stage = data['rawBlob']['product_stage']
    @product_section = data['rawBlob']['product_section']
    @product_group = data['rawBlob']['product_group']
    @milestone = data['rawBlob']['milestone']
  end

  def to_json(*args)
    SERIALIZED_ATTRIBUTES.dup.concat(SERIALIZED_METHODS).map { |method| [method.to_s, public_send(method)] }.to_h.to_json(*args)
  end

  def key
    File.basename(@data['name'].to_s, '.yml')
  end

  def distribution
    @data['rawBlob'].fetch('distributions', []).join(', ')
  end

  def tier
    tiers = @data['rawBlob'].fetch('tiers', [])

    if tiers.include?('free')
      'free'
    elsif tiers.include?('premium') && tiers.include?('ultimate')
      'premium'
    else
      'ultimate'
    end
  end

  def web_path
    return nil if @data['webPath'].nil?

    URI.join(GITLAB_BASE_URL, @data['webPath']).to_s
  end
end
