# frozen_string_literal: true

require 'csv'
require_relative 'loader'
require_relative 'parser'

class CsvGenerator
  def initialize
    @snowplow_data = generate_snowplow_data
    @service_ping_data = generate_service_ping_data
  end

  def generate
    write_csv(:service_ping)
    write_csv(:snowplow)
  end

  private

  attr_reader :snowplow_data, :service_ping_data

  def csv_data(data_type)
    data =
      if data_type == :service_ping
        service_ping_data
      else
        snowplow_data
      end

    data = data.map { |metric| metric['rawBlob'] }
    headers = data.first.keys.sort
    {
      data: data,
      headers: headers,
      filename: "web/static/data/#{data_type}.csv"
    }
  end

  def write_csv(data_type)
    csv_data = csv_data(data_type)

    return if csv_data[:data].empty?

    CSV.open(csv_data[:filename], 'w', headers: csv_data[:headers], write_headers: true) do |output_csv|
      csv_data[:data].each do |metric_data|
        output_csv << csv_data[:headers].map do |field|
          value = metric_data.fetch(field, '')
          value = value.join(', ') if value.is_a?(Array)
          value
        end
      end
    end
  end

  def generate_snowplow_data
    loader = Loader.new(type: :events)

    Parser.new(fetch_loader_data(loader)).parse
  end

  def generate_service_ping_data
    loader = Loader.new(type: :service_ping)

    Parser.new(fetch_loader_data(loader)).parse
  end

  def fetch_loader_data(loader)
    return loader.load if loader.fetched?

    data = loader.fetch
    loader.save
    data
  end
end
