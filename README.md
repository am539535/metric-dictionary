# Dictionary of the Service Ping metric definitions

With this dictionary we have an up-to-date collection of
GitLab's Service Ping metrics and Snowplow events available.
If you want to contribute to the project, please submit a
merge request and ping @gitlab-org/analytics-section/product-intelligence. 

The project can be viewed at: https://metrics.gitlab.com

## Development instructions

### Architecture

We query source data via GitLab's GraphQL and REST APIs, parse
the YML files and generate JSON files to be rendered in a
Javascript powered [table](https://gridjs.io/), rendered using [Nuxt](https://nuxtjs.org/).

The pages are rebuilt daily with fresh data at 9:00 AM UTC.

### How to develop locally

After checking out the repository, you can run the following commands to generate the metric dictionary page and open it:

```sh
bin/seed
bin/build
# Nuxt project lives at /web
yarn --cwd ./web
yarn --cwd ./web dev
```

To open the project's console, run:

```sh
bin/console
```
### Review

At this moment we don't have a big volume of contributions to this project.

In order to have MRs reviewed, share a message in [#g_product_intelligence](https://gitlab.slack.com/archives/CL3A7GFPF) channel or ask a [Product Intelligence team member](https://gitlab-org.gitlab.io/gitlab-roulette/?sortKey=localTime&visible=reviewer%7Cproduct+intelligence) for a review. 
